import java.util.Date;

public abstract class Snack extends Product implements comparable{
	public Snack(double calories, double quanitySize, 
			java.util.Date manufacturingAndExpiryDate) {
		super(calories, quanitySize, manufacturingAndExpiryDate);
			}

	public String name;
	public double calories;
	public double size;
	public java.util.Date manufacturingAndExpiryDate;
	
	public String toString(){
		return "Snack";
	}
}
abstract class Candy extends Snack{
	
	public Candy(double calories, double quanitySize, 
			java.util.Date manufacturingAndExpiryDate) {
		super(calories, quanitySize, manufacturingAndExpiryDate);
			}
	
	
	String name;
	double calories;
	double size;
	String manufacturingAndExpiryDate;
	double cost;
	double weight;
	String category;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getCalories() {
		return calories;
	}
	public void setCalories(double calories) {
		this.calories = calories;
	}
	public double getSize() {
		return size;
	}
	public void setSize(double size) {
		this.size = size;
	}
	public String getManufacturingAndExpiryDate1() {
		return manufacturingAndExpiryDate;
	}
	public void setManufacturingAndExpiryDate(String manufacturingAndExpiryDate) {
		this.manufacturingAndExpiryDate = manufacturingAndExpiryDate;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
				
	}	

abstract class Chips extends Snack{
	
	public Chips(double calories, double quanitySize,
			java.util.Date manufacturingAndExpiryDate) {
		super(calories, quanitySize, manufacturingAndExpiryDate);
			}
	
	String name;
	double calories;
	double size;
	String manufacturingAndExpiryDate;
	double cost;
	double weight;
	String category;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getCalories() {
		return calories;
	}
	public void setCalories(double calories) {
		this.calories = calories;
	}
	public double getSize() {
		return size;
	}
	public void setSize(double size) {
		this.size = size;
	}
	public String getManufacturingAndExpiryDate1() {
		return manufacturingAndExpiryDate;
	}
	public void setManufacturingAndExpiryDate(String manufacturingAndExpiryDate) {
		this.manufacturingAndExpiryDate = manufacturingAndExpiryDate;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
abstract class Chocolate extends Snack{
	
	public Chocolate(double calories, double quanitySize,
			java.util.Date manufacturingAndExpiryDate) {
		super(calories, quanitySize, manufacturingAndExpiryDate);
			}
	
	String name;
	double calories;
	double size;
	String manufacturingAndExpiryDate;
	double cost;
	double weight;
	String category;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getCalories() {
		return calories;
	}
	public void setCalories(double calories) {
		this.calories = calories;
	}
	public double getSize() {
		return size;
	}
	public void setSize(double size) {
		this.size = size;
	}
	public String getManufacturingAndExpiryDate1() {
		return manufacturingAndExpiryDate;
	}
	public void setManufacturingAndExpiryDate(String manufacturingAndExpiryDate) {
		this.manufacturingAndExpiryDate = manufacturingAndExpiryDate;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
}
}
	

