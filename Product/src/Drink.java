import java.util.Random;

public class Drink extends Product implements comparable {
	
	public Drink(double calories, double quanitySize, 
			java.util.Date manufacturingAndExpiryDate) {
		super(calories, quanitySize, manufacturingAndExpiryDate);
			}

	
	public double calories;
	public String name;
	public double quantitySize;
	public java.util.Date manufacturingAndExpiryDate;
	

	 public double getCalories() {
		return calories;
	}


	public void setCalories(double calories) {
		this.calories = calories;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public double getQuantitySize() {
		return quantitySize;
	}


	public void setQuantitySize(double quantitySize) {
		this.quantitySize = quantitySize;
	}


	public java.util.Date getManufacturingAndExpiryDate() {
		return manufacturingAndExpiryDate;
	}


	public void setManufacturingAndExpiryDate(java.util.Date manufacturingAndExpiryDate) {
		this.manufacturingAndExpiryDate = manufacturingAndExpiryDate;
	}


	public String toString(){
		 return "Drink";
	 }
	

public String celebrate() 
{
	int r = new Random().nextInt(4);
	String message = "";
	switch(r){
	
case 0: message = " Great Choice";break;
case 1: message = " I would have selected the other one";break;
case 2: message = " Wow realy!";break;
case 3: message = " Enjoy your day";break;
case 4: message = " Be blessed";break;
	}
	
return message;
	}
}



