import java.util.Date;

public interface comparable{
	public double getCalories();
	
	public void setCalories(double calories);

	public String getName();

	public void setName(String name); 
	
	public double getQuantitySize();
	
	public void setQuantitySize(double quantitySize);
	
	public Date getManufacturingAndExpiryDate();
	
	public void setManufacturingAndExpiryDate(java.util.Date manufacturingAndExpiryDate);
	}


